const configUrl = chrome.runtime.getURL('config.json');

const createResultElement = (story, mergeRequests, customFields) => {
  if ((customFields && (customFields.customfield_10401 || customFields.customfield_10400)) || mergeRequests) {
    let jiraCustoRoot = document.createElement(`div`);
    jiraCustoRoot.className = 'jira-custo-root';
    let pointsElem = document.createElement(`div`);
    pointsElem.className = 'jira-custo-points';
    let mergeRequestsElem = document.createElement(`div`);
    mergeRequestsElem.className = 'jira-custo-MR';
    if (customFields && customFields.customfield_10401) {
      let frontendPointsElem = document.createElement(`div`);
      frontendPointsElem.className = 'jira-custo-frontend-points';
      let frontendPointsTextElem = document.createTextNode(`Points frontend: `);
      frontendPointsElem.appendChild(frontendPointsTextElem);
      let frontendPointsValue = document.createElement('b');
      frontendPointsValue.innerText = customFields.customfield_10401;
      frontendPointsElem.appendChild(frontendPointsValue);
      pointsElem.appendChild(frontendPointsElem);
    }

    if (customFields && customFields.customfield_10400) {
      let backendPointsElem = document.createElement(`div`);
      backendPointsElem.className = 'jira-custo-backend-points';
      let backendPointsTextElem = document.createTextNode(`Points backend: `);
      backendPointsElem.appendChild(backendPointsTextElem);
      let backendPointsValue = document.createElement('b');
      backendPointsValue.innerText = customFields.customfield_10400;
      backendPointsElem.appendChild(backendPointsValue);
      pointsElem.appendChild(backendPointsElem);
    }

    if (customFields && customFields.customfield_11001) {
      let wpfPointsElem = document.createElement(`div`);
      wpfPointsElem.className = 'jira-custo-wpf-points';
      let wpfPointsTextElem = document.createTextNode(`Points WPF: `);
      wpfPointsElem.appendChild(wpfPointsTextElem);
      let wpfPointsValue = document.createElement('b');
      wpfPointsValue.innerText = customFields.customfield_11001;
      wpfPointsElem.appendChild(wpfPointsValue);
      pointsElem.appendChild(wpfPointsElem);
    }
    
    jiraCustoRoot.appendChild(pointsElem);

    if (customFields && customFields.issuelinks) {
      let links = customFields.issuelinks.filter(link => link.type.name === 'Blocks' && (link.inwardIssue?.fields.status.id === '3' || link.inwardIssue?.fields.status.id === '10100' || link.inwardIssue?.fields.status.id === '10205'));
      if (links.length > 0) {
        jiraCustoRoot.classList.add('blocked');
      }
    }

    if (mergeRequests && mergeRequests.length >= 0) {
      let opened = mergeRequests.filter(o => o.state === 'opened').length;
      let merged = mergeRequests.filter(o => o.state === 'merged').length;
      let closed = mergeRequests.filter(o => o.state === 'closed').length;

      let openedElem = document.createElement(`span`);
      openedElem.className = 'jira-custo-opened';
      let openedtextElem = document.createTextNode(`MR opened: ${opened}`);
      openedElem.appendChild(openedtextElem);

      let mergedElem = document.createElement(`span`);
      mergedElem.className = 'jira-custo-merged';
      let mergedtextElem = document.createTextNode(`merged: ${merged}`);
      mergedElem.appendChild(mergedtextElem);

      let closedElem = document.createElement(`span`);
      closedElem.className = 'jira-custo-closed';
      let closedtextElem = document.createTextNode(`closed: ${closed}`);
      closedElem.appendChild(closedtextElem);

      mergeRequestsElem.appendChild(openedElem);
      mergeRequestsElem.appendChild(mergedElem);
      // mergeRequestsElem.appendChild(closedElem);
    }
    jiraCustoRoot.appendChild(mergeRequestsElem);

    let issueFields = story.getElementsByClassName('ghx-issue-fields')[0];
    let issueSummaryElement = story.getElementsByClassName('ghx-summary')[0];
    issueFields.insertBefore(jiraCustoRoot, issueSummaryElement);
  }
}

const run = async (config) => {
  const stories = document.getElementsByClassName('ghx-issue')
  const { gitlabUrl, gitlabToken, currentUser, jiraUrl, jiraLogin, jiraPassword } = config
  for (const storyKey in stories) {
    const story = stories[storyKey];
    if (!(story && story.getElementsByClassName && typeof story.getElementsByClassName === 'function')) {
      break;
    }
    let issueKey = story.dataset['issueKey']
    let avatars = story.getElementsByClassName('ghx-avatar-img');

    const jiraCustomFieldsUrl = `${jiraUrl}/issue/${issueKey}?fields=customfield_10400,customfield_10401,customfield_11001,issuelinks`;
    let headers = new Headers();
    headers.set('Authorization', 'Basic ' + btoa(jiraLogin + ":" + jiraPassword));
    const response = await fetch(jiraCustomFieldsUrl, {
      method: 'GET',
      headers: headers
    })
    const json = await response.json();
    const customFields = json.fields;
    if (avatars && avatars[0] && avatars[0].alt && avatars[0].alt.includes(currentUser)) {
      let url = `${gitlabUrl}/merge_requests?search=${issueKey}&private_token=${gitlabToken}`;

      fetch(url)
        .then(function (response) {
          return response.json();
        })
        .then(function (mergeRequests) {
          createResultElement(story, mergeRequests, customFields);
        });
    } else {
      createResultElement(story, false, customFields);
    }
  }
};
let config = null;
fetch(configUrl)
  .then((response) => response.json())
  .then((configJSon) => {
    config = Object.assign({}, configJSon);
    run(config)
  });


const reloadButton = document.createElement('BUTTON');
reloadButton.className = 'jira-custo-reload-button';
reloadButton.innerHTML = "Reload MR status";
reloadButton.onclick = () => {
  run(config);
}
document.getElementById('ghx-modes-tools').appendChild(reloadButton);